.. PythonTop40 documentation master file, created by
   sphinx-quickstart on Sun Nov 30 21:43:04 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PythonTop40 Project Documentation v |global_version_full|
=========================================================

.. sectionauthor:: Danny Goodall <danny@onebloke.com>

Contents
--------
.. toctree::
   :maxdepth: 2

   installation
   moredetail
   top40
   errors
   changes

.. include:: ../README.rst


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

