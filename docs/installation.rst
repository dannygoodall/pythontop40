Installation
============

**PythonTop40** can be found on the Python Package Index `PyPi here. <https://pypi.python.org/pypi/pythontop40>`_
It can be installed using ``pip``, like so. ::

    pip install pythontop40